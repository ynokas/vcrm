<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(MenusTableSeeder::class);
        $this->call(DishesTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(DishProductTableSeeder::class);
        $this->call(DishMenuTableSeeder::class);
    }
}

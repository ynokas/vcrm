<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                'name' => 'Audrius',
                'email' => 'delliks@gmail.com',
                'password' => bcrypt('321321')
            ]
        );
        DB::table('users')->insert(
            [
                'name' => 'Rutkus',
                'email' => 'rutkus@gmail.com',
                'password' => bcrypt('321321')
            ]
        );
    }
}

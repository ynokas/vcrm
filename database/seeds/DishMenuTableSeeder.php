<?php

use Illuminate\Database\Seeder;

class DishMenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dish_menu')->insert(
            [
                'dish_id' => '1',
                'menu_id' => '1'
            ]);
        DB::table('dish_menu')->insert(
            [
                'dish_id' => '2',
                'menu_id' => '1'
            ]);
        DB::table('dish_menu')->insert(
            [
                'dish_id' => '3',
                'menu_id' => '1'
            ]);
        DB::table('dish_menu')->insert(
            [
                'dish_id' => '1',
                'menu_id' => '2'
            ]);
        DB::table('dish_menu')->insert(
            [
                'dish_id' => '2',
                'menu_id' => '2'
            ]);
        DB::table('dish_menu')->insert(
            [
                'dish_id' => '3',
                'menu_id' => '3'
            ]);
    }
}

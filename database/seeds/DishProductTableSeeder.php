<?php

use Illuminate\Database\Seeder;

class DishProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dish_product')->insert(
            [
                'dish_id' => '1',
                'product_id' => '1'
            ]);
        DB::table('dish_product')->insert(
            [
                'dish_id' => '2',
                'product_id' => '1'
            ]);
        DB::table('dish_product')->insert(
            [
                'dish_id' => '3',
                'product_id' => '1'
            ]);
        DB::table('dish_product')->insert(
            [
                'dish_id' => '1',
                'product_id' => '2'
            ]);
        DB::table('dish_product')->insert(
            [
                'dish_id' => '2',
                'product_id' => '2'
            ]);
        DB::table('dish_product')->insert(
            [
                'dish_id' => '3',
                'product_id' => '3'
            ]);
    }
}

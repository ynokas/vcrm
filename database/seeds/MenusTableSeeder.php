<?php

use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->insert(
            [
                'name' => 'Pirmadienis',
                'user_id' => '1',
                'comment' => 'Pusryčiai'
            ]
        );
        DB::table('menus')->insert(
            [
                'name' => 'Trečiadienis',
                'user_id' => '1',
                'comment' => 'Pietūs'
            ]
        );
        DB::table('menus')->insert(
            [
                'name' => 'Ketvirtadienis',
                'user_id' => '1',
                'comment' => 'Dienos pietūs'
            ]
        );
    }
}

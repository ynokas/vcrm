<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert(
            [
                'name' => 'Kiaušiniai',
                'comment' => 'Kaimiški',
				'unit' => 'vnt',
				'price_single' => '0.189'
            ]);
        DB::table('products')->insert(
            [
                'name' => 'Miltai',
                'comment' => 'Kvietiniai',
				'unit' => 'gr',
				'price_single' => '0.00059'
            ]);
        DB::table('products')->insert(
            [
                'name' => 'Pienas',
                'comment' => 'Riebus',
				'unit' => 'ml',
				'price_single' => '0.002'
            ]
        );
		DB::table('products')->insert(
			[
				'name' => 'Vištiena',
				'comment' => 'Šlaunelės',
				'unit' => 'gr',
				'price_single' => '0.006'
			]
		);
    }
}

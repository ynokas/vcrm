<?php

use Illuminate\Database\Seeder;

class DishesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dishes')->insert(
            [
                'name' => 'Blynai',
                'user_id' => '1',
                'comment' => 'Miltiniai'
            ]);
        DB::table('dishes')->insert(
            [
                'name' => 'Kiaušinienė',
                'user_id' => '1',
                'comment' => 'Su sūriu'
            ]);
        DB::table('dishes')->insert(
            [
                'name' => 'Vištienos kepsnys',
                'user_id' => '1',
                'comment' => 'Dienos pietums'
            ]
        );
    }
}

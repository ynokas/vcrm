<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('menus.index');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::apiResource('menus', 'MenuController');
    Route::post('/menu/{menu}/attach', 'MenuController@attach')->name('menus.attach');
    Route::post('/menu/{menu}/detach', 'MenuController@detach')->name('menus.detach');
    Route::apiResource('dishes', 'DishController');
    Route::post('/dish/{dish}/attach', 'DishController@attach')->name('dishes.attach');
    Route::post('/dish/{dish}/detach', 'DishController@detach')->name('dishes.detach');
	Route::post('/dish/{dish}/images', 'DishController@upload')->name('dishes.upload');

	Route::apiResource('products', 'ProductController');
});

<?php

namespace App\Http\Controllers;

use App\Dish;
use App\Menu;
use Illuminate\Http\Request;
use App\Repositories\MenuRepository;

class MenuController extends Controller
{
    protected $menus;

    public function __construct(MenuRepository $menus)
    {
        $this->menus = $menus;
    }

    public function index(Request $request)
    {

        return view('menus.index', [
                'menus' => $this->menus->forUser($request->user()),
            ]);
    }

    public function show(Menu $menu)
    {
        $dishes = $menu->dishes()->orderBy('created_at', 'desc')->paginate(10);

        $array = [];
        foreach ($dishes as $dish)
            $array[] = $dish->id;

        $all_dishes = Dish::all()->except($array);

        return view('menus.show', compact('menu','dishes','all_dishes'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255'
        ]);

        $request->user()->menus()->create(
            [
                'name' => $request->name,
                'comment' => $request->comment,
            ]
        );

        return redirect()->route('menus.index');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255'
        ]);

        $menu = Menu::find($id);

        $menu->name = $request->name;
        $menu->comment = $request->comment;
        $menu->save();

        return redirect()->route('menus.show', $request->id);
    }


    public function destroy(Menu $menu)
    {
        $menu->delete();

        return redirect()->route('menus.index');
    }

    public function detach(Request $request, Menu $menu)
    {
        $this->validate($request, [
            'dish' => 'integer|required|exists:dishes,id'
        ]);

        $dish = Dish::find($request->get('dish'));
        $menu->dishes()->detach($dish);

        return redirect()->route('menus.show', $menu->id);
    }

    public function attach(Request $request, Menu $menu)
    {
        $this->validate($request, [
            'dish' => 'integer|required|exists:dishes,id'
        ]);

        $dish = Dish::find($request->get('dish'));
        $menu->dishes()->attach($dish);

        return redirect()->route('menus.show', $menu->id);
    }
}

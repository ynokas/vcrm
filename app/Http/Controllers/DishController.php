<?php

namespace App\Http\Controllers;

use App\Dish;
use App\Product;
use Illuminate\Http\Request;
use App\Repositories\DishRepository;
use Symfony\Component\Finder\SplFileInfo;

class DishController extends Controller
{
    protected $dishes;

    public function __construct(DishRepository $dishes)
    {
        $this->dishes = $dishes;
    }

    public function index(Request $request)
    {
        return view('dishes.index', [
                'dishes' => $this->dishes->forUser($request->user()),
            ]);
    }

    public function show(Dish $dish)
    {
        $products = $dish->products()->orderBy('created_at', 'desc')->paginate(10);

        $array = [];
        foreach ($products as $product)
            $array[] = $product->id;

        $all_products = Product::all()->except($array);

        $all_images = \File::allFiles('storage/images');
		foreach ($all_images as $key => $image)
		{
			$all_images2[$key]["name"] = $image->getRelativePathname();
			$all_images2[$key]["path"] = $image->getPath();
        }

        return view('dishes.show', compact('dish','products','all_products','all_images2'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255'
        ]);

        $request->user()->dishes()->create(
            [
                'name' => $request->name,
                'comment' => $request->comment,
				'image' => $request->image
            ]
        );

        return redirect()->route('dishes.index');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255'
        ]);

        $dish = Dish::find($id);

        $dish->name = $request->name;
        $dish->comment = $request->comment;
        $dish->image = $request->image;
        $dish->save();

        return redirect()->back();
    }

    public function destroy(Dish $dish)
    {
        $dish->delete();

        return redirect()->route('dishes.index');
    }

    public function detach(Request $request, Dish $dish)
    {
        $this->validate($request, [
            'product' => 'integer|required|exists:products,id'
        ]);

        $product = Product::find($request->get('product'));
        $dish->products()->detach($product);

        return redirect()->route('dishes.show', $dish->id);
    }

    public function attach(Request $request, Dish $dish)
    {
        $this->validate($request, [
            'product' => 'integer|required|exists:products,id'
        ]);

        $product = Product::find($request->get('product'));

        $amount = $request->get('amount-'.$request->get('product'));

		$dish->products()->attach($product, array('amount' => $amount));


        return redirect()->route('dishes.show', $dish->id);
    }

    public function upload(Request $request)
	{
		$name = $request->get('name');
		$ext = $request->file('image')->guessExtension();
		$request->file('image')->storeAs('public/images', $name.'.'.$ext);

		return redirect()->back();
	}

}

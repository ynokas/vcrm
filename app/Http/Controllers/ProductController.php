<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Repositories\ProductRepository;

class ProductController extends Controller
{
    public function index()
    {
        return view('products.index', [
                'products' => Product::all(),
            ]);
    }

    public function show(Product $product)
    {
        $dishes = $product->dishes()->orderBy('created_at', 'desc')->paginate(1);

        return view('products.show', compact('product','dishes'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255'
        ]);

        Product::create([
                'name' => $request->name,
                'comment' => $request->comment,
				'unit' => $request->unit,
				'price' => $request->price_single
        ])->save();

        return redirect()->route('products.index');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255'
        ]);

        $product = Product::find($id);

        $product->name = $request->name;
        $product->comment = $request->comment;
        $product->unit = $request->unit;
        $product->price_single = $request->price_single;
        $product->save();

        return redirect()->route('products.show', $request->id);
    }

    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('products.index');
    }
}

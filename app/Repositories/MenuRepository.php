<?php
namespace App\Repositories;

use App\User;

class MenuRepository
{
    public function forUser(User $user)
    {
        return $user->menus()->orderBy('created_at', 'desc')->get();
    }
}
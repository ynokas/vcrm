<?php
namespace App\Repositories;

use App\User;

class DishRepository
{
    public function forUser(User $user)
    {
        return $user->dishes()->orderBy('created_at', 'desc')->get();
    }
}
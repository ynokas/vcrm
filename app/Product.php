<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	const UNIT_GRAMS = 'gr';
	const UNIT_LITRE = 'ml';
	const UNIT_UNIT = 'vnt';

    protected $fillable = [
        'name', 'comment', 'unit', 'price_single'
    ];

    public function dishes()
    {
        return $this->belongsToMany(Dish::class);
    }

}

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Meniukas {{ $menu->name }}</div>
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form action="{{ route('menus.update', $menu->id) }}" method="post" class="form-horizontal">
                            {{ method_field('PUT') }}
                            {{ csrf_field() }}

                            <div class="form-group {{$errors->has('name') ? ' has-error' : '' }}"}}>
                                <label for="name" class="col-sm-3 control-label">Meniu pavadinimas</label>
                                <div class="col-sm-6">
                                    <input type="text" name="name" id="name" value="{{ $menu->name }}" class="form-control">

                                    @if ($errors->has('name'))
                                        <div class="help-block">
                                            Iveskite pavadinima
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{$errors->has('name') ? ' has-error' : '' }}"}}>
                                <label for="comment" class="col-sm-3 control-label">Komentaras</label>
                                <div class="col-sm-6">
                                    <input type="text" name="comment" id="comment" value="{{ $menu->comment }}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-6">
                                    <button type="submit" class="btn btn-default">Išsaugoti</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Prisegtų patiekalų sąrašas:</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if ($dishes->count())
                            <table class="table table-striped">
                                <thead>
                                <th>Pavadinimas</th>
                                <th>Komentaras</th>
                                <th>&nbsp;</th>
                                </thead>
                                <tbody>
                                <form action="{{ route('menus.detach', $menu->id)}} " method="post">
                                    {{ csrf_field() }}
                                @foreach ($dishes as $dish)
                                    <tr>
                                        <td> <a href="{{route('dishes.show', $dish->id) }}">{{ $dish->name }}</a></td>
                                        <td> {{ $dish->comment }}</td>
                                        <td>
                                                <button name="dish" value="{{$dish->id}}" type="submit" class="btn btn-danger">Atsegti</button>
                                        </td>
                                    </tr>
                                @endforeach
                                </form>
                                </tbody>
                            </table>
                        @else
                            <p> Patiekalas neturi prisegtų patiekalų </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Prisegti kitą patiekalą:</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if ($all_dishes->count())
                            <table class="table table-striped">
                                <thead>
                                <th>Pavadinimas</th>
                                <th>Komentaras</th>
                                <th>&nbsp;</th>
                                </thead>
                                <tbody>
                                <form action="{{ route('menus.attach', $menu->id)}} " method="post">
                                    {{ csrf_field() }}
                                @foreach ($all_dishes as $dish)
                                    <tr>
                                        <td> <a href="{{route('dishes.show', $dish->id) }}">{{ $dish->name }}</a></td>
                                        <td> {{ $dish->comment }}</td>
                                        <td>
                                            <button name="dish" value="{{$dish->id}}" type="submit" class="btn btn-danger">Prisegti</button>
                                        </td>
                                    </tr>
                                @endforeach
                                </form>
                                </tbody>
                            </table>
                        @else
                            <p> Nera kitų patiekalų </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

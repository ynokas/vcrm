@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Meniukai</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form action="{{ route('menus.store') }}" method="post" class="form-horizontal">
                            {{ csrf_field() }}
                            <div class="form-group {{$errors->has('name') ? ' has-error' : '' }}"}}>
                                <label for="name" class="col-sm-3 control-label">Meniu pavadinimas</label>
                                <div class="col-sm-6">
                                    <input type="text" name="name" id="name" class="form-control">

                                    @if ($errors->has('name'))
                                        <div class="help-block">
                                            Įveskite pavadinimą
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{$errors->has('name') ? ' has-error' : '' }}"}}>
                                <label for="comment" class="col-sm-3 control-label">Komentaras</label>
                                <div class="col-sm-6">
                                    <input type="text" name="comment" id="comment" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-6">
                                    <button type="submit" class="btn btn-default">Pridėti</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Sąrašas:</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if ($menus->count())
                            <table class="table table-striped">
                                <thead>
                                    <th>Pavadinimas</th>
                                    <th>Komentaras</th>
                                    <th>&nbsp;</th>
                                </thead>
                                <tbody>
                                    @foreach ($menus as $menu)
                                        <tr>
                                            <td> <a href="{{route('menus.show', $menu->id) }}"> {{ $menu->name }} </a></td>
                                            <td> {{ $menu->comment }}</td>
                                            <td>
                                                <form action="{{ route('menus.destroy', $menu->id)}} " method="post">
                                                    <button type="submit" class="btn btn-danger">Trinti</button>
                                                    {{ method_field('DELETE') }}
                                                    {{ csrf_field() }}
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                        <p> Neturite Meniuku </p>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Produktas {{ $product->name }}</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form action="{{ route('products.update', $product->id) }}" method="post" class="form-horizontal">
                            {{ method_field('PUT') }}
                            <div class="form-group {{$errors->has('name') ? ' has-error' : '' }}"}}>
                                <label for="name" class="col-sm-3 control-label">Produkto pavadinimas</label>
                                <div class="col-sm-6">
                                    <input type="text" name="name" id="name" value="{{ $product->name }}" class="form-control">
                                    @if ($errors->has('name'))
                                        <div class="help-block">
                                            Įveskite pavadinimą
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-row {{$errors->has('name') ? ' has-error' : '' }}"}}>
                                <div class="col-md-4">
                                    <label for="price">Kaina (Eur)</label>
                                    <input type="number" name="price" id="price" placeholder="2.50" step="0.01" class="form-control value-input">
                                </div>
                                <div class="col-md-4">
                                    <label for="amount">Kiekis (už kainą)</label>
                                    <input type="number" name="amount" id="amount" placeholder="100" class="form-control value-input">
                                </div>
                                <div class="col-md-4">
                                    <label for="unit">Matavimo vienetas</label>
                                    <select name="unit" id="unit" class="form-control">
                                        <option id="1" name="gr" value="gr" {{$product->unit == 'gr' ? 'selected' : ''}}>Gramai</option>
                                        <option id="2" name="ml" value="ml" {{$product->unit == 'ml' ? 'selected' : ''}}>Litrai</option>
                                        <option id="3" name="vnt" value="vnt" {{$product->unit == 'vnt' ? 'selected' : ''}}>Vienetai</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group {{$errors->has('name') ? ' has-error' : '' }}"}}>
                                <label for="price_single" class="col-sm-3 control-label">Vieneto kaina (Eur)</label>
                                <div class="col-sm-3">
                                    <input type="number" step="0.000000001" name="price_single" id="price_single" value="{{ $product->price_single }}" class="form-control">
                                    @if ($errors->has('price_single'))
                                        <div class="help-block">
                                            Įveskite kainą už vienetą
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{$errors->has('name') ? ' has-error' : '' }}"}}>
                                <label for="comment" class="col-sm-3 control-label">Komentaras</label>
                                <div class="col-sm-6">
                                    <input type="text" name="comment" id="comment" value="{{ $product->comment }}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-6">
                                    <button type="submit" class="btn btn-default">Išsaugoti</button>
                                </div>
                            </div>
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Produktas prisegtas prie šių patiekalų: </div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if ($dishes->count())
                            <table class="table table-striped">
                                <thead>
                                <th>Pavadinimas</th>
                                <th>Komentaras</th>
                                {{--<th>&nbsp;</th>--}}
                                </thead>
                                <tbody>
                                @foreach ($dishes as $dish)
                                    <tr>
                                        <td> <a href="{{route('products.show', $dish->id) }}">{{ $dish->name }}</a></td>
                                        <td> {{ $dish->comment }}</td>
                                        {{--<td>--}}
                                            {{--<form action="{{ route('products.destroy', $dish->id)}} " method="post">--}}
                                                {{--<button type="submit" class="btn btn-danger">Trinti</button>--}}
                                                {{--{{ method_field('DELETE') }}--}}
                                                {{--{{ csrf_field() }}--}}
                                            {{--</form>--}}
                                        {{--</td>--}}
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <p> Patiekalų nerasta</p>
                        @endif


                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var priceInput = document.querySelector('#price')
        var amountInput = document.querySelector('#amount')
        var priceSingleInput = document.querySelector('#price_single')
        var inputs = document.querySelectorAll('.value-input')

        for (var i = 0; i < inputs.length; i++) {
            inputs[i].addEventListener('input', function () {
                priceSingleInput.value = (priceInput.value / amountInput.value).toFixed(9)
            })
        }
    </script>
@endsection

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Produktai</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form action="{{ route('products.store') }}" method="post" class="form-horizontal">
                            <div class="form-group {{$errors->has('name') ? ' has-error' : '' }}"}}>
                                <label for="name" class="col-sm-3 control-label">Produkto pavadinimas</label>
                                <div class="col-sm-6">
                                    <input type="text" name="name" id="name" class="form-control">

                                    @if ($errors->has('name'))
                                        <div class="help-block">
                                            Iveskite pavadinima
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{$errors->has('name') ? ' has-error' : '' }}"}}>
                                <label for="comment" class="col-sm-3 control-label">Komentaras</label>
                                <div class="col-sm-6">
                                    <input type="text" name="comment" id="comment" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-6">
                                    <button type="submit" class="btn btn-default">Pridėti</button>
                                </div>
                            </div>
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Sąrašas:</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if ($products->count())
                            <table class="table table-striped">
                                <thead>
                                    <th>Pavadinimas</th>
                                    <th>Komentaras</th>
                                    <th>Vieneto kaina</th>
                                    <th>&nbsp;</th>
                                </thead>
                                <tbody>
                                    @foreach ($products as $product)
                                        <tr>
                                            <td> <a href="{{route('products.show', $product->id) }}"> {{ $product->name }} </a></td>
                                            <td> {{ $product->comment }}</td>
                                            <td> {{ number_format($product->price_single, 3) }} Eur/{{ $product->unit }}</td>
                                            <td>
                                                <form action="{{ route('products.destroy', $product->id)}} " method="post">
                                                    <button type="submit" class="btn btn-danger">Trinti</button>
                                                    {{ method_field('DELETE') }}
                                                    {{ csrf_field() }}
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                        <p> Neturite produktų </p>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

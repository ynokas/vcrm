@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Patiekalas {{ $dish->name }}</div>
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form action="{{ route('dishes.update', $dish->id) }}" method="post" class="form-horizontal">
                            {{ method_field('PUT') }}
                            {{ csrf_field() }}
                            <div class="form-group {{$errors->has('name') ? ' has-error' : '' }}"}}>
                                <label for="name" class="col-sm-3 control-label">Patiekalo pavadinimas</label>
                                <div class="col-sm-6">
                                    <input type="text" name="name" id="name" value="{{ $dish->name }}" class="form-control">

                                    @if ($errors->has('name'))
                                        <div class="help-block">
                                            Įveskite pavadinimą
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{$errors->has('name') ? ' has-error' : '' }}"}}>
                                <label for="comment" class="col-sm-3 control-label">Komentaras</label>
                                <div class="col-sm-6">
                                    <input type="text" name="comment" id="comment" value="{{ $dish->comment }}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="image" class="col-sm-3 control-label">Animacija</label>
                                <div class="col-sm-6">
                                    <select title="Paveiksliukas" name="image" class="selectpicker">
                                        <option>Pasirinkti...</option>
                                        @foreach ($all_images2 as $image)
                                            <option data-thumbnail="{{ asset($image["path"].'/'.$image["name"]) }}" value="{{ $image["name"] }}">{{ $image["name"] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-6">
                                    <button type="submit" class="btn btn-default">Išsaugoti</button>
                                </div>
                            </div>
                        </form>
                    </div>
                <div class="panel-footer">
                    <div class="form-group-sm" align="center">
                        <form method="POST" action="{{ route('dishes.upload', $dish->id) }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <label for="image">Atsiųsti animaciją/nuotrauką:</label>
                            <input type="file" name="image" id="image" class="alert-info">
                            Pavadinimas: <input type="text" name="name">
                            <button type="submit">Siųsti</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body" style="text-align: center">
                        @if ($dish->image)
                            <img src="{{ asset('storage/images/'.$dish->image) }}" class="animation">
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Prisegtų produktų sąrašas: <p align="right">
                            @php
                             $dish_price = 0.0;
                             foreach ($products as $product)
                                $dish_price += $product->price_single * $product->pivot->amount;
                            @endphp
                            Patiekalo kaina: {{$dish_price}} Eur
                        </p></div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if ($products->count())
                            <table class="table table-striped">
                                <thead>
                                <th>Pavadinimas</th>
                                <th>Komentaras</th>
                                <th>Kiekis</th>
                                <th>Vieneto kaina</th>
                                <th>Bendra kaina</th>
                                <th>&nbsp;</th>
                                </thead>
                                <tbody>
                                <form action="{{ route('dishes.detach', $dish->id)}} " method="post">
                                    {{ csrf_field() }}
                                @foreach ($products as $product)
                                    <tr>
                                        <td> <a href="{{route('products.show', $product->id) }}">{{ $product->name }}</a></td>
                                        <td> {{ $product->comment }}</td>
                                        <td> {{ $product->pivot->amount }} {{ $product->unit }} </td>
                                        <td> {{ $product->price_single }} Eur/{{$product->unit}}</td>
                                        <td> {{ $product->price_single * $product->pivot->amount }} Eur</td>
                                        <td>
                                            <button name="product" value="{{$product->id}}" type="submit" class="btn btn-danger">Atsegti</button>
                                        </td>
                                    </tr>
                                @endforeach
                                </form>
                                </tbody>
                            </table>
                        @else
                            <p> Patiekalas neturi prisegtų produktų </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Prisegti kitą produktą:</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if ($all_products->count())
                            <table class="table table-striped">
                                <thead>
                                <th>Pavadinimas</th>
                                <th>Komentaras</th>
                                <th>Kiekis</th>
                                <th>&nbsp;</th>
                                </thead>
                                <tbody>
                                <form action="{{ route('dishes.attach', $dish->id)}}" method="post">
                                    {{ csrf_field() }}
                                @foreach ($all_products as $product)
                                    <tr>
                                        <td> <a href="{{route('products.show', $product->id) }}">{{ $product->name }}</a></td>
                                        <td> {{ $product->comment }}</td>
                                        <td>
                                            <div class="number-input">
                                                <input type="number" class="form-control input-sm" name="amount-{{ $product->id }}" id="amount-{{ $product->id }}">
                                                <span>{{ $product->unit }}</span>
                                            </div>
                                        </td>
                                        <td>
                                            <button name="product" value="{{$product->id}}" type="submit" class="btn btn-danger">Prisegti</button>
                                        </td>
                                    </tr>
                                @endforeach
                                </form>
                                </tbody>
                            </table>
                        @else
                            <p> Nėra kitų produktų </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
